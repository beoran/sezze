package asm

// We use a properly configured CSV as an assembler parser.
// It seems strange but it is easy!
import "encoding/csv"

import "io"
import "strings"
import "strconv"
import "fmt"

import "codeberg.org/beoran/sezze/scpu"

type ASM struct {
	Offset int
	Size   int
	ROM    [scpu.ROMSize]scpu.Word
	Labels map[string]scpu.Addr
	*csv.Reader
}

func New(rd io.Reader) *ASM {
	res := &ASM{}
	res.Labels = make(map[string]scpu.Addr)
	res.Reader = csv.NewReader(rd)
	res.Reader.Comma = ' '
	res.Reader.Comment = '#'
	res.Reader.FieldsPerRecord = -1
	res.Reader.TrimLeadingSpace = true
	res.Reader.LazyQuotes = true
	return res
}

func (a ASM) Write(wr io.Writer) error {
	var buf [2]byte
	for i := 0; i < a.Size && i < len(a.ROM); i++ {
		buf[0] = byte(a.ROM[i] & 255)
		buf[1] = byte(a.ROM[i] >> 8)
		_, err := wr.Write(buf[:])
		if err != nil {
			return err
		}
	}
	return nil
}

func reg(reg string) scpu.Word {
	switch strings.ToUpper(reg) {
	case "R0":
		return 0
	case "R1":
		return 1
	case "R2":
		return 2
	case "R3":
		return 3
	case "R4":
		return 4
	case "R5":
		return 5
	case "R6":
		return 6
	case "R7":
		return 7
	case "R8":
		return 8
	case "R9":
		return 9
	case "R10":
		return 10
	case "R11":
		return 11
	case "R12":
		return 12
	case "R13":
		return 13
	case "R14":
		return 14
	case "R15":
		return 15
	case "NUL":
		return scpu.NUL
	case "IP":
		return scpu.IP
	case "SP":
		return scpu.SP
	default:
		return -1
	}
}

func mkop(opcode scpu.Opcode, args []string) (scpu.Word, error) {
	if len(args) < 4 {
		return 0, fmt.Errorf("Opcode needs 3 operands %d", args[0])
	}
	sdr, ss1, ss2 := args[1], args[2], args[3]
	dr := reg(sdr)
	s1 := reg(ss1)
	s2 := reg(ss2)
	if dr < 0 || s1 < 0 || s2 < 0 {
		return 0, fmt.Errorf("Unknown register %s %s %s", sdr, ss1, ss2)
	}
	return scpu.Word(opcode)<<12 | dr<<8 | s1<<4 | s2, nil
}

func (a ASM) constant(sc1 string) (scpu.Word, error) {
	if sc1[0] >= 'a' && sc1[0] <= 'z' || sc1[0] == '_' {
		addr, ok := a.Labels[sc1]
		if !ok {
			return 0, fmt.Errorf("Unknown label %s", sc1)
		}
		return scpu.Word(addr), nil
	}
	if len(sc1) > 0 && sc1[0] == '\'' {
		r, _, _, err := strconv.UnquoteChar(sc1, '\'')
		if err != nil {
			return 0, err
		}
		return scpu.Word(r), nil
	}
	i, err := strconv.ParseInt(sc1, 0, 16)
	if err != nil {
		return 0, err
	}
	return scpu.Word(i), nil
}

func iconstant(sc1 string) (int, error) {
	if len(sc1) > 0 && sc1[0] == '\'' {
		r, _, _, err := strconv.UnquoteChar(sc1, '\'')
		if err != nil {
			return 0, err
		}
		return int(r), nil
	}
	i, err := strconv.ParseInt(sc1, 0, 64)
	if err != nil {
		return 0, err
	}
	return int(i), nil
}

func (a ASM) mkopconst(opcode scpu.Opcode, args []string) (scpu.Word, error) {
	if len(args) < 3 {
		return 0, fmt.Errorf("Opcode needs 2 operands %d", args[0])
	}
	sdr, sc1 := args[1], args[2]

	dr := reg(sdr)
	c1, err := a.constant(sc1)
	if err != nil {
		return 0, err
	}
	if dr < 0 {
		return 0, fmt.Errorf("Unknown register %s", sdr)
	}
	return scpu.Word(opcode)<<12 | dr<<8 | c1, nil
}

func insconst(args []string) (int, error) {
	if len(args) < 2 {
		return 0, fmt.Errorf("Instruction needs 1 operand %d", args[0])
	}
	return iconstant(args[1])
}

func (a *ASM) AssembleOnce() error {
	var i int

	rec, err := a.Read()
	if err != nil {
		return err
	}
	if len(rec) < 1 {
		return nil
	}

	switch rec[0] {
	case "ADD":
		a.ROM[a.Offset], err = mkop(scpu.ADD, rec)
	case "SUB":
		a.ROM[a.Offset], err = mkop(scpu.SUB, rec)
	case "AND":
		a.ROM[a.Offset], err = mkop(scpu.AND, rec)
	case "OR":
		a.ROM[a.Offset], err = mkop(scpu.OR, rec)
	case "XOR":
		a.ROM[a.Offset], err = mkop(scpu.XOR, rec)
	case "SHL":
		a.ROM[a.Offset], err = mkop(scpu.SHL, rec)
	case "SHR":
		a.ROM[a.Offset], err = mkop(scpu.SHR, rec)
	case "LDW":
		a.ROM[a.Offset], err = mkop(scpu.LDW, rec)
	case "STW":
		a.ROM[a.Offset], err = mkop(scpu.STW, rec)
	case "CAL":
		a.ROM[a.Offset], err = mkop(scpu.CAL, rec)
	case "RET":
		a.ROM[a.Offset], err = mkop(scpu.RET, rec)
	case "JMP":
		a.ROM[a.Offset], err = mkop(scpu.JMP, rec)
	case "LCL":
		a.ROM[a.Offset], err = a.mkopconst(scpu.LCL, rec)
	case "LCH":
		a.ROM[a.Offset], err = a.mkopconst(scpu.LCH, rec)
	case "HALT":
		args := []string{"HALT", "NUL", "0x8"}
		a.ROM[a.Offset], err = a.mkopconst(scpu.LCL, args)
	case ".label":
		if len(rec) < 2 {
			err = fmt.Errorf("label needs name")
		} else {
			a.Labels[rec[1]] = scpu.Addr(a.Offset)
			a.Offset--
		}
	case ".def":
		if len(rec) < 3 {
			err = fmt.Errorf("def needs name and value")
		} else {
			var v scpu.Word
			v, err = a.constant(rec[2])
			a.Labels[rec[1]] = scpu.Addr(v)
			a.Offset--
		}
	case ".dw":
		i, err = insconst(rec)
		a.ROM[a.Offset] = scpu.Word(i)
	case ".offset":
		a.Offset, err = insconst(rec)
		a.Offset--
	case ".size":
		a.Size, err = insconst(rec)
		a.Size--
	default:
		err = fmt.Errorf("Unknown instruction: %s", rec[0])
	}
	if err != nil {
		return err
	}
	a.Offset++
	a.Size++
	return nil
}

func (a *ASM) Assemble() error {
	var err error
	for err = a.AssembleOnce(); err == nil; err = a.AssembleOnce() {
	}
	if err == io.EOF {
		return nil
	}
	row, col := a.Reader.FieldPos(0)
	fmt.Println("On %d %d", row, col)
	return err
}
