// This is a mini assembler to assemble SEZZE rims.
package main

import "os"
import "fmt"

import "codeberg.org/beoran/sezze/asm"

func main() {
	if len(os.Args) < 3 {
		fmt.Println("Please specify ASM and ROM")
		os.Exit(1)
	}

	fin, err := os.Open(os.Args[1])

	if err != nil {
		fmt.Printf("Error opening ASM: %s\n", err)
		os.Exit(2)
	}
	defer fin.Close()

	asm := asm.New(fin)
	err = asm.Assemble()
	if err != nil {
		fmt.Printf("Error assemling ASM: %s\n", err)
		os.Exit(4)
	}
	rom, err := os.Create(os.Args[2])
	if err != nil {
		fmt.Printf("Error creating ROM: %s\n", err)
		os.Exit(6)
	}
	defer rom.Close()

	err = asm.Write(rom)
	if err != nil {
		fmt.Printf("Error writing ROM: %s\n", err)
		os.Exit(8)
	}
}
