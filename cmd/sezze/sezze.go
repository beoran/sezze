package main

import "codeberg.org/beoran/sezze/scpu"

import "os"
import "fmt"

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Please specify ROM")
		os.Exit(1)
	}
	cpu := scpu.NewCPU()
	err := cpu.LoadROMFileName(os.Args[1])
	if err != nil {
		fmt.Printf("Error loading ROM: %s\n", err)
		os.Exit(2)
	}
	cpu.Start()
}
