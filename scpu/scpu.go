package scpu

import "fmt"
import "io"
import "os"

const RAMSize = (1<<14 - 1)
const StackSize = (1<<12 - 1)
const RetSize = (1<<8 - 1)
const DEVSize = 1 << 10
const DEVRam = 1 << 16
const ROMSize = (1<<15 - 1)
const DEVCount = 16
const REGCount = 16
const DEVPrint = 8
const DEVMMU = 7
const PrintRAMSize = 512

type Addr uint16
type Word int16
type UWord Addr

const (
	TopRAM Addr = RAMSize
	TopDEV Addr = TopRAM + DEVSize*DEVCount
	TopROM Addr = TopDEV + ROMSize
)

type Device interface {
	MPut(addr Addr, val Word)
	MGet(addr Addr) (val Word)
}

type MMU struct {
	Trap    func(addr Addr)
	RET     [RetSize]Word
	STACK   [StackSize]Word
	RAM     [RAMSize]Word
	Devices [16]Device
	ROM     [ROMSize]Word
}

func (m *MMU) MPut(addr Addr, value Word) {
	if addr < TopRAM {
		m.RAM[addr] = value
		return
	}
	if addr < TopDEV {
		di := (addr - TopRAM) >> 10
		m.Devices[di].MPut(addr, value)
		return
	}
	// Cannot write to ROM.
	return
}

func (m MMU) MGet(addr Addr) (val Word) {
	if addr < TopRAM {
		return m.RAM[addr]
	}
	if addr < TopDEV {
		di := (addr - TopRAM) >> 10
		sub := addr - TopRAM - di*DEVSize
		return m.Devices[di].MGet(sub)
	}
	return m.ROM[addr-TopDEV]
}

type Regs struct {
	R [REGCount]Word
}

type Flag Word

const (
	FlagCarry Flag = 1 << iota
	FlagCond
	FlagIRQ
	FlagHalt
)

func (r Regs) IsFlag(f Flag) bool {
	return (Flag(r.R[FL]) & f) == f
}

func (r Regs) Cond() bool {
	return r.IsFlag(FlagCarry)
}

type CPU struct {
	Regs
	MMU
}

type NullDevice struct{}

func (NullDevice) MPut(addr Addr, val Word) {
}

func (NullDevice) MGet(addr Addr) (val Word) {
	return 0
}

// Print device for debugging the emulator.
type PrintDevice struct {
	RAM [PrintRAMSize]rune
}

func (t *PrintDevice) MPut(addr Addr, val Word) {
	if addr < Addr(len(t.RAM)) {
		t.RAM[addr] = rune(val)
	} else {
		start := addr - PrintRAMSize
		fmt.Printf(string(t.RAM[start:val]))
	}
}

func (t PrintDevice) MGet(addr Addr) (val Word) {
	if addr < Addr(len(t.RAM)) {
		return Word(t.RAM[addr])
	}
	return 0
}

func MakeMMU() MMU {
	res := MMU{}
	for i := 0; i < len(res.Devices); i++ {
		res.Devices[i] = NullDevice{}
	}
	res.Devices[DEVMMU] = &res
	res.Devices[DEVPrint] = &PrintDevice{}
	return res
}

func NewCPU() *CPU {
	res := &CPU{}
	res.MMU = MakeMMU()
	// On start up registers are pointing to the lowest addres of the RAM.
	res.SetRegs(0)
	return res
}

func (c *CPU) SetRegs(offset int) {
	for i := 0; i < 4; i++ {
		c.R[i] = &c.MMU.RAM[i] // global registers are at the bottom of memory.
	}
	for i := 4; i < len(c.R); i++ {
		c.R[i] = &c.MMU.RAM[i+offset] // others are at an offset.
	}
}

type Opcode uint8

const (
	ADD Opcode = iota // DR,S1,S2 | DR <- S1 + S2         | sets cond if DST!=0
	ADC               // DR,S1,S2 | DR <- S1 + S2 + carry | carry, sets carry and cond if DST!=0
	SUB               // DR,S1,S2 | DR <- S1 + S2         | sets cond if DST=0
	SBC               // DR,S1,S2 | DR <- S1 - S2 - carry | sets carry and cond if DST!=0
	AND               // DR,S1,S2 | DR <- S1 & S2         | sets cond if DST!=0
	OR                // DR,S1,S2 | DR <- S1 | S2         | sets cond if DST!=0
	XOR               // DR,S1,S2 | DR <- S1 ^ S2         | sets cond if DST!=0
	SHL               // DR,S1,S2 | DR <- S1 << S2        | sets cond if DST!=0
	SHR               // DR,S1,S2 | DR <- S1 >> S2        | sets cond if DST!=0
	SRA               // DR,S1,S2 | DR <- S1 >>>S2        | sets cond if DST!=0
	LDW               // DR,S1,S2 | DR <- M[S1+S2]        | loads signed
	LWU               // DR,S1,S2 | DR <- M[S1+S2]        | loads unsigned
	LDB               // DR,S1,S2 | DR <- M[S1+S2]        | loads low byte sign extended
	LBU               // DR,S1,S2 | DR <- M[S1+S2]        | loads low byte unsigned
	STW               // DR,S1,S2 | M[S1+S2] <- DR        | stores word
	STB               // DR,S1,S2 | M[S1+S2] <- DR        | stores register
	LCL               // DR,C1    | DR<7:0> <- C1         | loads low byte of word from constant
	LCH               // DR,C1    | DR<16:8> <- C1        | loads high byte of word from constant
	JMP               // DR,C1    | IP <- DR + C1         | jumps absolute if cond is set
	JMR               // DR,C1    | IP <- IP + DR + C1    | jumps relative if cond is set
	CAL               // DR,C1    | IP <- DR + C1         | RS <- IP + 1; RS++  before call
	CLR               // DR,C1    | IP <- IP + DR + C1    | RS <- IP + 1; RS++ before call
	RET               // DR,C1    | IP <- RS              | RS-- before return
	PSH               // DR,C1    | STACK[SP] <- RS + C1  | SP++ after push
	POP               // DR,C1    | RS <- STACK[SP+C1]    | SP-- after push
	SFL               // DR,C1    | FLAGS <- DR | C1      | Set FLAGS register.
	EX0               // DR,C1    |                       | Reserved.
	EX1               // DR,C1    |                       | Reserved.
	EX2               // DR,C1    |                       | Reserved.
	EX3               // DR,C1    |                       | Reserved.
	EX4               // DR,C1    |                       | Reserved.
	EX5               // DR,C1    |                       | Reserved.
)

const (
	NUL = 0
	R1  = 1
	R2  = 2
	R3  = 3
	R4  = 4
	R5  = 5
	R6  = 6
	R7  = 7
	IP  = 8
	SP  = 9
	RP  = 10
	FL  = 11
	R12 = 12
	R13 = 13
	R14 = 14
	R15 = 15
)

func (c *CPU) Start() {
	*c.R[NUL] = 0           // NUL register
	*c.R[IP] = Word(TopDEV) // IP
	*c.R[SP] = 0            // SP
	*c.R[FL] = 0            // FL
	*c.R[RP] = 0            // RP
	c.Run()
}

/*
Decode an Addr/Uword as an opcode:
- OPCODE(5) DR(3) IMM(1) S1(3) S2(4) (OPCODE < LCL)
- OPCODE(5) DR(3) C1(8) (OPCODE > LCL)
*/
func (w UWord) Decode() (code Opcode, dr, imm, s1, s2, c1 UWord) {
	code = Opcode((w & 0b111110000000000) >> 11)
	s1 = (w & 0b0000011100000000) >> 8
	if code < LCL {
		imm = (w & 0b10000000) >> 7
		s1 = (w & 0b01110000) >> 4
		s2 = (w & 0b00001111)
	} else {
		c1 = (w & 0b11111111)
	}
	return code, dr, imm, s1, s2, c1
}

// Encode an instruction as a UWord
func Encode(code Opcode, dr, imm, s1, s2, c1 UWord) UWord {
	var w UWord
	w = UWord(code) << 11
	w = w & ((s1 & 0b0111) << 8)
	if code < LCL {
		w = w & ((imm & 0b1) << 7)
		w = w & ((s1 & 0b111) << 4)
		w = w & (s2 & 0b1111)
	} else {
		w = w & (c1 & 0b11111111)
	}
	return w
}

func (c *CPU) Step() {
	var u1, u2 UWord
	var w1, w2 Word

	instruction := UWord(c.MGet(Addr(*c.R[IP])))
	opcode, dr, imm, s1, s2, c1 := instruction.Decode()

	w1 = *c.R[s1]
	if imm == 1 {
		w2 = Word(s2)
	} else {
		w2 = *c.R[s2]
	}

	u1 = UWord(w1)
	u2 = UWord(w2)

	switch opcode {
	case ADD:
		if dr > 0 {
			*c.R[dr] = w1 + w2
			if c.Carry {
				*c.R[dr]++
			}
		} else {
			c.Cond = (*c.R[s1] + *c.R[s2]) != 0
		}
	case ADC:
		if dr > 0 {
			*c.R[dr] = w1 + w2
			if c.Carry {
				*c.R[dr]++
			}
		} else {
			c.Cond = (*c.R[s1] + *c.R[s2]) != 0
		}
	case SUB:
		if dr > 0 {
			*c.R[dr] = *c.R[s1] + *c.R[s2]
			if c.Carry {
				*c.R[dr]--
			}
		} else {
			c.Cond = (*c.R[s1] + *c.R[s2]) != 0
		}
	case AND:
		if dr > 0 {
			*c.R[dr] = *c.R[s1] & *c.R[s2]
		} else {
			c.Cond = (*c.R[s1] & *c.R[s2]) != 0
		}
	case OR:
		if dr > 0 {
			*c.R[dr] = *c.R[s1] | *c.R[s2]
		} else {
			c.Cond = (*c.R[s1] | *c.R[s2]) != 0
		}
	case XOR:
		if dr > 0 {
			*c.R[dr] = *c.R[s1] ^ *c.R[s2]
		} else {
			c.Cond = (*c.R[s1] ^ *c.R[s2]) != 0
		}
	case SHL:
		if dr > 0 {
			*c.R[dr] = *c.R[s1] << *c.R[s2]
			if c.Carry {
				*c.R[dr]++
			}
		} else {
			c.Cond = (*c.R[s1] << *c.R[s2]) != 0
		}
	case SHR:
		if dr > 0 {
			*c.R[dr] = *c.R[s1] << *c.R[s2]
			if c.Carry {
				*c.R[dr]++
			}
		} else {
			c.Cond = (*c.R[s1] << *c.R[s2]) != 0
		}
	case LDW:
		*c.R[dr] = c.MGet(Addr(*c.R[s1]) + Addr(*c.R[s2]))
	case STW:
		c.MPut(Addr(*c.R[dr])+Addr(*c.R[s2]), *c.R[s1])
	case CAL:
		*c.R[SP] += 8
		c.SetRegs(int(*c.R[SP]))
		if dr > 0 {
			*c.R[dr] = *c.R[SP]
		} else {
			c.IRQ = true
		}
		*c.R[IP] = *c.R[s1] + c1
	case RET:
		*c.R[SP] -= 8
		c.SetRegs(int(*c.R[SP]))
		if dr > 0 {
			*c.R[dr] = *c.R[SP]
		} else {
			c.IRQ = false
		}
		*c.R[IP] = *c.R[s1] + c1
	case JMP:
		if c.Cond {
			if dr > 0 {
				*c.R[dr] = *c.R[IP]
			} else {
				c.Cond = false
			}
			*c.R[IP] = *c.R[s1] + c1
		}
	case LCL:
		if dr > 0 {
			*c.R[dr] = c1
		} else {
			c.Carry = (c1 & 1) == 1
			c.Cond = (c1 & 2) == 2
			c.IRQ = (c1 & 4) == 4
			c.Halt = (c1 & 8) == 8
		}
	case LCH:
		if dr > 0 {
			*c.R[dr] = *c.R[dr] | c1<<8
		} else {
			c.Carry = (c1 & 1) != 1
			c.Cond = (c1 & 2) != 2
			c.IRQ = (c1 & 4) != 4
			c.Halt = (c1 & 8) != 8
		}
	default:
		c.Halt = true
		panic("unknown instruction")
	}
	*c.R[IP]++
	c.Halt = Addr(*c.R[IP]) >= TopROM
}

func (c *CPU) Run() {
	// TODO: timing.
	for !c.Halt {
		c.Step()
	}
}

func (c *CPU) LoadROMBuffer(buf []byte) {
	for i := 1; i < len(c.ROM) && i < len(buf); i += 2 {
		c.ROM[i>>1] = Word(buf[i])<<8 + Word(buf[i-i])
	}
}

func (c *CPU) LoadROMReader(rd io.Reader) error {
	buf, err := io.ReadAll(rd)
	if err != nil {
		return err
	}
	c.LoadROMBuffer(buf)
	return nil
}

func (c *CPU) LoadROMFileName(fn string) error {
	fin, err := os.Open(fn)
	if err != nil {
		return err
	}
	defer fin.Close()
	return c.LoadROMReader(fin)
}
