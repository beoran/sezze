# ISA


# SCPU-16

16 bits RISC LOAD/STORE architecture, with a stack machine for locals,
arguments and function returns.

16 registers, of which 8 are directly writable.
The other 8 are readable as the second operand of
several instructions, but only indirectly writable.
R0 always is 0 on read, no effect on write.
R1-R7 are normal ALU registers.

R8: FLAGS (condition, carry, irq )
R9: SP: Stack pointer. The built in stack is a 1k on CPU stack.
R10: IP: instruction pointer
R11: RP: return stack pointer. The return stack is an on CPU 128 word stack.
R12: IR: interrupt register.
R13:
R14:
R15:
R16:

Negative: 2 complements notation. Registers are normally considered signed
containing a int16 WORD. Except, for LDW or STW instructions, then the
WORD is converted to an uint16 ADDR.

SCPU-16 memory is word-addressed to double range (64kw = 128kb)
While word processing is less efficient for ASCII text processing,
it is more efficient for other purposes, such as graphics, or for processing
oriental languages.

Format (high to low)

- OPCODE(5) DR(3) IMM(1) S1(3) S2(4) (OPCODE < LCL)
- OPCODE(5) DR(3) C1(8) (OPCODE > LCL)


If IMM =1 then s4 is a immediate nibble. IF imm == 0 it is possible to read from
16 registers using S2. The DR can only be 0-7 to allow for more instructions
and a immediate mode.

C1 is signed.

Instructions:

ADD      | DR,S1,S2 | DR <- S1 + S2         | sets cond if DST!=0
ADC      | DR,S1,S2 | DR <- S1 + S2 + carry | carry, sets carry and cond if DST!=0
SUB      | DR,S1,S2 | DR <- S1 + S2         | sets cond if DST=0
SBC      | DR,S1,S2 | DR <- S1 - S2 - carry | sets carry and cond if DST!=0
AND      | DR,S1,S2 | DR <- S1 & S2         | sets cond if DST!=0
OR       | DR,S1,S2 | DR <- S1 | S2         | sets cond if DST!=0
XOR      | DR,S1,S2 | DR <- S1 ^ S2         | sets cond if DST!=0
SHL      | DR,S1,S2 | DR <- S1 << S2        | sets cond if DST!=0
SHR      | DR,S1,S2 | DR <- S1 >> S2        | sets cond if DST!=0
SRA      | DR,S1,S2 | DR <- S1 >>>S2        | sets cond if DST!=0
LDW      | DR,S1,S2 | DR <- M[S1+S2]        | loads signed
LWU      | DR,S1,S2 | DR <- M[S1+S2]        | loads unsigned
LDB      | DR,S1,S2 | DR <- M[S1+S2]        | loads low byte sign extended
LBU      | DR,S1,S2 | DR <- M[S1+S2]        | loads low byte unsigned
STW      | DR,S1,S2 | M[S1+S2] <- DR        | stores word
STB      | DR,S1,S2 | M[S1+S2] <- DR        | stores register

LCL      | DR,C1    | DR<7:0> <- C1         | loads low byte of word from constant
LCH      | DR,C1    | DR<16:8> <- C1        | loads high byte of word from constant
JMP      | DR,C1    | PC <- DR + C1         | jumps absolute if cond is set
JMR      | DR,C1    | PC <- PC + DR + C1    | jumps relative if cond is set
CAL      | DR,C1    | PC <- DR + C1         | RS <- PC + 1; RS++  before call
CLR      | DR,C1    | PC <- PC + DR + C1    | RS <- PC + 1; RS++ before call
RET      | DR,C1    | PC <- RS              | RS-- before return
PSH      | DR,C1    | STACK[SP] <- RS + C1  | SP++ after push
POP      | DR,C1    | RS <- STACK[SP+C1]    | SP-- after push
SFL      | DR,C1    | FLAGS <- DR | C1      | Set FLAGS register.
EX0      | DR,C1    |                       | Reserved.
EX1      | DR,C1    |                       | Reserved.
EX2      | DR,C1    |                       | Reserved.
EX3      | DR,C1    |                       | Reserved.
EX4      | DR,C1    |                       | Reserved.
EX5      | DR,C1    |                       | Reserved.

All top 16 opcodes have a DR,S1,S2 format, all 16 bottom ones have a DR,C1 format.

Synthetic instructions:

NOP: ADD R0 R0 R0 (encoded as 0000)
MOV: ADD DR R0 SR is a MOV DR SR

## Memory Map

RAM (16kw=32kb). Stack and return stack are separate and on-CPU.

RAM is followeded by the SBUS: 16 device maps each 1kw or 2kb
TXT: text mode and fonts
VID: video device
AUD: audio device
STO: permanent storage device (disk,memory card, or SRAM)
INP: Input for joystics , mouse and keyboard.
MMU: Memory mapper, can be used to bank switch the ROM or RAM. Also DMA controller.
CLK: Real time clock and programmable clock. Also interrupt controller.
CRT: Cartridge may add device here.
DV0-DV7: 8 devices, for extension.

SBUS is followed by ROM of program card/cartridge.
ROM: 32kw=64kb,  32banks of 1kw or 2kb, may be bank switched by using MMU device
Devices are all 16 bytes but have their their own RAM/ROM up to 64kw = 128KB.
Cart may map 1 device in CRT. DV0-DV8 are reserved but can be connected through
the SBUS.




# RISC1

R0->R32
R0 is always 0, writing to it only causes flags to be set if requested.
R0-R9: global
R10*>r15 low
R16->r25 local
R26->r31 high.

Registers are windowed and the window is shifted.
The low registers of the caller become the high registers of the callee.
Registers have addresses as well.

ADD      | S1,S2,Rd | Rd <- S1 + S2
ADDC     | S1,S2,Rd | Rd <- S1 + S2 + carry
SUB      | S1,S2,Rd | Rd <- S1 - S2
SUBC     | S1,S2,Rd | Rd <- S1 - S2 - carry
AND      | S1,S2,Rd | Rd <- S1 & S2
OR       | S1,S2,Rd | Rd <- S1 | S2
XOR      | S1,S2,Rd | Rd <- S1 xor S2
SLL      | S1,S2,Rd | Rd <- S1 << S2
SRL      | S1,S2,Rd | Rd <- S1 >> S2
SRA      | S1,S2,Rd | Rd <- S1 >>> S2
LDL      | Rx,x, Rd | Rd <- M[Rx+X]
LDLU     | Rx,x, Rd | Rd <- M[Rx+X]
LDS      | Rx,x, Rd | Rd <- M[Rx+X]
LDSS     | Rx,x, Rd | Rd <- M[Rx+X]
LDBU     | Rx,x, Rd | Rd <- M[Rx+X]
LDBS     | Rx,x, Rd | Rd <- M[Rx+X]
STL      | Rx,x, Rd | M[Rx+X] <- Rn
STS      | Rx,x, Rd | M[Rx+X] <- Rn
STB      | Rx,x, Rd | M[Rx+X] <- Rn
JMP      | CND,X(Rm)| PC <- x + Rm
JMPR     | CND,y    | PC <- PC + Y
CALL     | Rm,X(rn) | Rm <- PC, next ; PC <- X+Rn ; CWP--
CALLR    | Rm, Y    | Rm <- PC, next ; PC <- pc+Y ; CWP--
RET      | Rm,X     | pc <- Rm+x, CWP++
LDHI     | Rm, y    | Rm<31:13> <- Y
GTLPC    | Rm       | Rm <- last PC
GTIN     | Rm       | Rm <- INR                          | Get interrupt number


Format:
- OPCODE(7) SETCOMP(1) DEST(5) SRC1(5) IMM(1) SRC2(13): most
- OPCODE(7) SETCOMP(1) DEST(5) OFFSET(19): for JMP, CALL, RET, ...


PSEUDO:
CMP: SUB Rm, Rn, R0
MOV: ADD R0, Rm, R1
TST: SUB Rn, r0, r0
CLR: ADD r0,r0,Rn
CLR: STL r0,r0,Rn
NEG
CMP
LDC
INC
DEC
IND





